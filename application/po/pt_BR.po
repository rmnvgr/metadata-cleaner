# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# Gustavo Costa <xfgusta@gmail.com>, 2021.
# Romain Vigier <romain@romainvigier.fr>, 2021.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021.
# Davi Patricio <davipatricio@pm.me>, 2021.
# Gabriel Gian <gabrielgian@live.com>, 2021.
# Daniel Abrante <danielabrante@protonmail.com>, 2023.
# Felipe Kinoshita <kinofhek@gmail.com>, 2023.
# Gabriel Camargo <g.camargo@proton.me>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-01 12:45+0000\n"
"PO-Revision-Date: 2023-10-12 03:00+0000\n"
"Last-Translator: Gabriel Camargo <g.camargo@proton.me>\n"
"Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/"
"metadata-cleaner/application/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.1-dev\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Limpador de metadados"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Limpe os metadados dos seus arquivos"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadata;Remover;Cleaner;Metadado;Removedor;Limpador;"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Veja e limpe os metadados em arquivos"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Os metadados em um arquivo podem dizer muito sobre você. As câmeras "
"registram dados sobre quando e onde uma foto foi tirada e qual câmera foi "
"usada. Os aplicativos de escritório adicionam automaticamente informações do "
"autor e da empresa a documentos e planilhas. Estas são informações "
"confidenciais e você pode não querer divulgá-las."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:28
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Esta ferramenta permite que você visualize metadados em seus arquivos e se "
"livre deles, o tanto quanto possível."

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "Arquivos"

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr "Adicionar arquivos"

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
msgid "Add folders"
msgstr "Adicionar pastas"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "Limpar metadados"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "Limpar todos os arquivos da janela"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "Geral"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "Nova janela"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "Fechar janela"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "Sair"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "Atalhos de teclado"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Ajuda"

#: application/data/ui/AddFilesButton.ui:21
msgid "_Add Files"
msgstr "_Adicionar arquivos"

#: application/data/ui/AddFilesButton.ui:33
msgid "Add _Folders"
msgstr "Adicionar _pastas"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Limpar"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Certifique-se de fazer backup dos seus arquivos!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Depois que os arquivos são limpos, não há como voltar atrás."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Não mostrar novamente"

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr "Cancelar"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Limpar"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "Limpe seus rastros"

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr "Saiba mais sobre metadados e as limitações do processo de limpeza"

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr "Remover arquivo da lista"

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr "Aviso"

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr "Erro"

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr "Limpo"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_Nova janela"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_Limpar janela"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_Ajuda"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "_Atalhos de teclado"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_Sobre o Limpador de metadados"

#: application/data/ui/MenuButton.ui:38
msgid "Main Menu"
msgstr "Menu Principal"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Configurações de limpeza"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "Limpeza leve"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Saiba mais sobre a limpeza leve"

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr "Detalhes"

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr "Fechar"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr ""
"Gustavo Costa <xfgusta@gmail.com>\n"
"Gabriel Gian <gabrielgian@protonmail.com>\n"
"Felipe Kinoshita <kinofhek@gmail.com>"

#: application/data/ui/Window.ui:128
msgid "Choose files to clean"
msgstr "Escolha os arquivos para limpar"

#: application/data/ui/Window.ui:138
msgid "Choose folders to clean"
msgstr "Escolha as pastas para limpar"

#: application/metadatacleaner/modules/file.py:249
msgid "An error occured during the cleaning."
msgstr "Um erro ocorreu durante a limpeza."

#: application/metadatacleaner/modules/file.py:252
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr "Algo ruim aconteceu durante a limpeza, arquivo limpo não encontrado"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr "O arquivo foi limpo"

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""
"Os metadados conhecidos foram removidos, contudo, o processo de limpeza tem "
"algumas limitações."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "Saiba mais"

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr "Não foi possível ler o arquivo"

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr "Tipo de arquivo não suportado"

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr "Não foi possível verificar os metadados"

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr "Sem metadados conhecidos"

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr "Não foi possível remover os metadados"

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr "O arquivo será limpo de qualquer maneira, para ter certeza."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Todos os arquivos suportados"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Adicionar arquivos de subpastas"

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr "Processando arquivo {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:39
msgid "Cleaning file {}/{}"
msgstr "Limpando arquivo {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:79
#, python-format
msgid "%i file cleaned."
msgid_plural "%i files cleaned."
msgstr[0] "%i arquivo limpo."
msgstr[1] "%i arquivos limpos."

#: application/metadatacleaner/ui/statusindicator.py:84
#, python-format
msgid "%i error occured."
msgid_plural "%i errors occured."
msgstr[0] "ocorreu %i erro."
msgstr[1] "ocorreram %i erros."

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr "Bibliotecas"

#~ msgid "Romain Vigier"
#~ msgstr "Romain Vigier"

#~ msgid "Clean without warning"
#~ msgstr "Limpar sem avisar"

#~ msgid "Clean the files without showing the warning dialog"
#~ msgstr "Limpar os arquivos sem mostrar o diálogo de aviso"

#~ msgid "Lightweight cleaning"
#~ msgstr "Limpeza leve"

#~ msgid "Don't make destructive changes to files but may leave some metadata"
#~ msgstr ""
#~ "Não faz alterações destrutivas aos arquivos, mas pode deixar alguns "
#~ "metadados"

#~ msgid "Window width"
#~ msgstr "Largura da janela"

#~ msgid "Saved width of the window"
#~ msgstr "Largura salva da janela"

#~ msgid "Window height"
#~ msgstr "Altura da janela"

#~ msgid "Saved height of the window"
#~ msgstr "Altura salva da janela"

#~ msgid "Updated translations"
#~ msgstr "Traduções atualizadas"

#~ msgid "Improved user interface"
#~ msgstr "Interface de usuário aprimorada"

#~ msgid "New translations"
#~ msgstr "Novas traduções"

#~ msgid "Bug fixes"
#~ msgstr "Correções de bugs"

#~ msgid "New button to add folders"
#~ msgstr "Novo botão para adicionar pastas"

#~ msgid "Improved adaptive user interface"
#~ msgstr "Interface de usuário adaptável aprimorada"

#~ msgid "New help pages"
#~ msgstr "Novas páginas de ajuda"

#~ msgid "One-click cleaning, no need to save after cleaning"
#~ msgstr "Limpar com um clique, não há necessidade de salvar após a limpeza"

#~ msgid "Persistent lightweight cleaning option"
#~ msgstr "Opção de limpeza leve e persistente"

#~ msgid "Files with uppercase extension can now be added"
#~ msgstr "Arquivos com extensão em maiúsculas agora podem ser adicionados"

#~ msgid "About"
#~ msgstr "Sobre"

#~ msgid "Chat on Matrix"
#~ msgstr "Chat no Matrix"

#~ msgid "View the code on GitLab"
#~ msgstr "Ver código no GitLab"

#~ msgid "Translate on Weblate"
#~ msgstr "Traduzir no Weblate"

#~ msgid "Support us on Liberapay"
#~ msgstr "Apoie-nos no Liberapay"

#~ msgid "Credits"
#~ msgstr "Créditos"

#~ msgid "Code"
#~ msgstr "Código"

#~ msgid "Artwork"
#~ msgstr "Arte"

#~ msgid "Documentation"
#~ msgstr "Documentação"

#~ msgid "Translation"
#~ msgstr "Tradução"

#~ msgid ""
#~ "This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
#~ "parse and clean the metadata. Show them some love!"
#~ msgstr ""
#~ "Este programa usa <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> "
#~ "para analisar e limpar os metadados. Ofereça-os um pouco de amor!"

#~ msgid ""
#~ "The source code of this program is released under the terms of the <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</"
#~ "a>. The original artwork and translations are released under the terms of "
#~ "the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA "
#~ "4.0</a>."
#~ msgstr ""
#~ "O código-fonte deste programa é lançado sob os termos da <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 ou "
#~ "posterior</a>. A arte original e as traduções são liberadas sob os termos "
#~ "<a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</"
#~ "a>."

#~ msgid "Adding files…"
#~ msgstr "Adicionando arquivos…"

#~ msgid "Croatian translation (contributed by Milo Ivir)"
#~ msgstr "Tradução croata (contribuição de Milo Ivir)"

#~ msgid "Portuguese (Brazil) translation (contributed by Gustavo Costa)"
#~ msgstr ""
#~ "Tradução para o Português brasileiro (contribuição de Gustavo Costa)"

#~ msgid "New in v1.0.4:"
#~ msgstr "Novo na versão 1.0.4:"

#~ msgid "Turkish translation (contributed by Oğuz Ersen)"
#~ msgstr "Tradução para o turco (contribuição de Oğuz Ersen)"

#~ msgid "New in v1.0.2:"
#~ msgstr "Novo na versão 1.0.2:"

#~ msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
#~ msgstr "Tradução para o espanhol (contribuição de Óscar Fernández Díaz)"

#~ msgid "Swedish translation (contributed by Åke Engelbrektson)"
#~ msgstr "Tradução para o sueco (contribuição de Åke Engelbrektson)"

#~ msgid "New in v1.0.1:"
#~ msgstr "Novo na versão 1.0.1:"

#~ msgid "German translation (contributed by lux)"
#~ msgstr "Tradução para o alemão (contribuição de lux)"

#~ msgid ""
#~ "Trade some metadata's presence in exchange of the guarantee that the data "
#~ "won't be modified"
#~ msgstr ""
#~ "Negocie a presença de alguns metadados em troca da garantia de que os "
#~ "dados não serão modificados"

#~ msgid "Warn before saving cleaned files"
#~ msgstr "Avisar antes de salvar os arquivos limpos"

#~ msgid "Show the warning dialog before saving the cleaned files"
#~ msgstr "Mostrar a janela de aviso antes de salvar os arquivos limpos"

#~ msgid ""
#~ "The GitLab, Matrix, Liberapay and Weblate logos and names are trademarks "
#~ "of their respective owners."
#~ msgstr ""
#~ "Os logotipos e nomes do GitLab, Matrix, Liberapay e Weblate são marcas "
#~ "registradas de seus respectivos proprietários."

#~ msgid "Note about metadata and privacy"
#~ msgstr "Nota sobre metadados e privacidade"

#~ msgid ""
#~ "Metadata consist of information that characterizes data. Metadata are "
#~ "used to provide documentation for data products. In essence, metadata "
#~ "answer who, what, when, where, why, and how about every facet of the data "
#~ "that are being documented.\n"
#~ "\n"
#~ "Metadata within a file can tell a lot about you. Cameras record data "
#~ "about when a picture was taken and what camera was used. Office "
#~ "aplications automatically add author and company information to documents "
#~ "and spreadsheets. Maybe you don't want to disclose those informations.\n"
#~ "\n"
#~ "This tool will get rid, as much as possible, of metadata."
#~ msgstr ""
#~ "Os metadados consistem em informações que caracterizam os dados. Os "
#~ "metadados são usados para fornecer documentação para produtos de dados. "
#~ "Em essência, os metadados respondem quem, o quê, quando, onde, por que e "
#~ "como sobre cada faceta dos dados que estão sendo documentados.\n"
#~ "\n"
#~ "Os metadados em um arquivo podem dizer muito sobre você. As câmeras "
#~ "registram dados sobre quando uma foto foi tirada e qual câmera foi usada. "
#~ "Os aplicativos de escritório adicionam automaticamente informações do "
#~ "autor e da empresa nos documentos e planilhas. Talvez você não queira "
#~ "divulgar essas informações.\n"
#~ "\n"
#~ "Esta ferramenta eliminará, o tanto quanto possível, os metadados."

#~ msgid ""
#~ "While this tool is doing its very best to display metadata, it doesn't "
#~ "mean that a file is clean from any metadata if it doesn't show any. There "
#~ "is no reliable way to detect every single possible metadata for complex "
#~ "file formats.\n"
#~ "\n"
#~ "This is why you shouldn't rely on metadata's presence to decide if your "
#~ "file must be cleaned or not."
#~ msgstr ""
#~ "Embora esta ferramenta esteja fazendo o seu melhor para exibir metadados, "
#~ "isso não significa que um arquivo está limpo de quaisquer metadados se "
#~ "não mostrar nenhum. Não há uma maneira confiável de detectar todos os "
#~ "metadados possíveis para formatos de arquivo complexos.\n"
#~ "\n"
#~ "É por isso que você não deve confiar na presença de metadados para "
#~ "decidir se seu arquivo deve ser limpo ou não."

#~ msgid ""
#~ "By default, the removal process might alter a bit the data of your files, "
#~ "in order to remove as much metadata as possible. For example, texts in "
#~ "PDF might not be selectable anymore, compressed images might get "
#~ "compressed again… If you're willing to trade some metadata's presence in "
#~ "exchange of the guarantee that the data of your files won't be modified, "
#~ "the lightweight mode precisely does that."
#~ msgstr ""
#~ "Por padrão, o processo de remoção pode alterar um pouco os dados de seus "
#~ "arquivos, a fim de remover o máximo de metadados possível. Por exemplo, "
#~ "textos em PDF podem não ser mais selecionáveis, imagens compactadas podem "
#~ "ser compactadas novamente... Se você estiver disposto a trocar a presença "
#~ "de alguns metadados em troca da garantia de que os dados de seus arquivos "
#~ "não serão modificados, o modo leve faz isso precisamente."

#~ msgid "Note about _metadata and privacy"
#~ msgstr "Nota sobre _metadados e privacidade"

#~ msgid "Note about _removing metadata"
#~ msgstr "Nota sobre _remoção de metadados"

#~ msgid "Metadata details"
#~ msgstr "Detalhes dos metadados"

#~ msgid "_Save"
#~ msgstr "_Salvar"

#~ msgctxt "shortcut window"
#~ msgid "Add files"
#~ msgstr "Adicionar arquivos"

#~ msgctxt "shortcut window"
#~ msgid "Save cleaned files"
#~ msgstr "Salvar arquivos limpos"

#~ msgid "Done!"
#~ msgstr "Feito!"

#~ msgid "Initializing…"
#~ msgstr "Inicializando…"

#~ msgid "Error while initializing the file parser."
#~ msgstr "Erro ao inicializar o analisador de arquivos."

#~ msgid "File type supported."
#~ msgstr "Tipo de arquivo compatível."

#~ msgid "Checking metadata…"
#~ msgstr "Verificando metadados…"

#~ msgid "Removing metadata…"
#~ msgstr "Removendo metadados…"

#~ msgid "Error while removing metadata:"
#~ msgstr "Erro ao remover metadados:"

#~ msgid "Saving the cleaned file…"
#~ msgstr "Salvando o arquivo limpo…"

#~ msgid "Error while saving the file:"
#~ msgstr "Erro ao salvar o arquivo:"

#~ msgid "The cleaned file has been saved."
#~ msgstr "O arquivo limpo foi salvo."

#~ msgid "{filename}:"
#~ msgstr "{filename}:"
